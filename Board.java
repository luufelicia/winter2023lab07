public class Board
{
	private Square[][] tictactoeBoard;
	
	public Board()
	{
		this.tictactoeBoard = new Square[][] { {Square.BLANK, Square.BLANK, Square.BLANK}, {Square.BLANK, Square.BLANK, Square.BLANK}, {Square.BLANK, Square.BLANK, Square.BLANK} };
	}
	
	public String toString()
	{
		String boardString = "";
		for (int i = 0; i<tictactoeBoard.length; i++)
		{
			for (int j = 0; j<tictactoeBoard[i].length; j++)
			{
				boardString+=tictactoeBoard[i][j] + " ";
			}
			boardString+="\n";
		}
		return boardString;
	}
	
	public boolean placeToken(int row, int col, Square playerToken)
	{
		if ((row<0 || row>2) || (col<0 || col>2))
		{
			return false;
		}
		
		if (this.tictactoeBoard[row][col] == Square.BLANK)
		{
			this.tictactoeBoard[row][col] = playerToken;
			return true;
		}
		
		else
		{
			return false;
		}
	}
	
	public boolean checkIfFull()
	{
		for (int i = 0; i<tictactoeBoard.length; i++)
		{
			for (int j = 0; j<tictactoeBoard[i].length; j++)
			{
				if (tictactoeBoard[i][j] == Square.BLANK)
				{
					return false;
				}
			}
		}
		return true;
	}
	
	//tictactoeBoard[row][column]: to check horizontal, the variable is the row so you do [i][constant nums]
	private boolean checkIfWinningHorizontal(Square playerToken)
	{
		for (int i = 0; i<tictactoeBoard.length; i++)
		{
			if ((this.tictactoeBoard[i][0]==playerToken) && (this.tictactoeBoard[i][1]==playerToken) && (this.tictactoeBoard[i][2]==playerToken))
			{
				return true;
			}
		}
		return false;
	}
	
	//tictactoeBoard[row][column]: to check vertical, the variable is the column so you do [constant nums][i].
	private  boolean checkIfWinningVertical(Square playerToken)
	{
		for (int i = 0; i<tictactoeBoard.length; i++)
		{
			if ((this.tictactoeBoard[0][i]==playerToken) && (this.tictactoeBoard[1][i]==playerToken) && (this.tictactoeBoard[2][i]==playerToken))
			{
				return true;
			}
		}
		return false;
	}
	
	public boolean checkIfWinning(Square playerToken)
	{
		if (checkIfWinningHorizontal(playerToken) == true || checkIfWinningVertical(playerToken) == true)
		{
			return true;
		}
		return false;
	}
	
}