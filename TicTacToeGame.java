import java.util.Scanner;
public class TicTacToeGame
{
	//To teacher: I did not do the bonus, so it won't work if one of the players win diagonally.
	public static void main(String[] args)
	{
		System.out.println("Hello, welcome to the Tic-Tac-Toe Game!");
		
		Board gameBoard = new Board();
		boolean gameOver = false;
		int player = 1;
		//player 1's token is X, player 2's token is O.
		Square playerToken = Square.X;
		
		System.out.println("Player 1's token is X");
		System.out.println("Player 2's token is O");
		int row = 0;
		int col = 0;
		
		Scanner scan = new Scanner(System.in);
		while (gameOver!=true)
		{
			System.out.println(gameBoard);
			if (player==1)
			{
				playerToken = Square.X;
			}
			else
			{
				playerToken = Square.O;
			}
			
			System.out.println("It's player " + player + "'s turn. Where would you like to place the token?");
			row = scan.nextInt();
			col = scan.nextInt();
			System.out.println("____");
			
			while (gameBoard.placeToken(row, col, playerToken) == false)
			{
				System.out.println("Invalid values! The place is taken or the values are out of range (0-2). Please enter again.");
				row = scan.nextInt();
				col = scan.nextInt();
				System.out.println("____");
			}
			
			if (gameBoard.checkIfFull() == true)
			{
				System.out.println("It's a tie!");
				gameOver=true;
			}
			
			else if (gameBoard.checkIfWinning(playerToken) == true)
			{
				System.out.println("Player " + player + " is the winner!!! Congratulations! :D");
				break;
			}
			
			else
			{
				player+=1;
				if (player>2)
				{
					player=1;
				}
			}
		}
	}
}